package com.itau.jogo21.business;

import java.util.List;

import com.itau.jogo21.dtos.Baralho;
import com.itau.jogo21.dtos.Carta;

//TODO: lançar excessão ao jogar jogo encerrado
public class Jogo {
  private Baralho baralho = new Baralho();
  private int pontuacao = 0;
  private boolean estaEncerrado = false;
  private boolean eVitoria = false;
  
  public List<Carta> jogarPrimeiraRodada() {
    List<Carta> cartas = baralho.sortear(2);
    
    for(Carta carta : cartas) {
      pontuacao += carta.getRank().getPontuacao();
    }
    
    return cartas;
  }
  
  public Carta jogar() {
    Carta carta = baralho.sortear();
    
    pontuacao += carta.getRank().getPontuacao();
    
    verificarFinalizacao();
    
    return carta;
  }
  
  private void verificarFinalizacao() {
    if(pontuacao >= 21) {
      estaEncerrado = true;
      eVitoria = pontuacao == 21;
    }
  }
  
  public int getPontuacao() {
    return pontuacao;
  }
  
  public boolean estaEncerrado() {
    return estaEncerrado;
  }
  
  public boolean eVitoria() {
    return eVitoria;
  }
}
