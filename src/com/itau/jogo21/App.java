package com.itau.jogo21;

import java.util.List;

import com.itau.jogo21.business.Jogo;
import com.itau.jogo21.dtos.Baralho;
import com.itau.jogo21.dtos.Carta;
import com.itau.jogo21.view.Console;

public class App {
  public static void main(String[] args) {
    Jogo jogo = new Jogo();
    
    List<Carta> cartasPrimeiraRodada = jogo.jogarPrimeiraRodada();
    Boolean querJogar = true;
    
    Console.imprimirJogada(cartasPrimeiraRodada, jogo);
    
    querJogar = Console.querJogar();
    
    while(!jogo.estaEncerrado() && querJogar) {
      Carta carta = jogo.jogar();
      
      Console.imprimirJogada(carta, jogo);
      
      if(!jogo.estaEncerrado()){
        querJogar = Console.querJogar();
      }
    }
    
    Console.imprimirJogoFinalizado(jogo);
  }
}
